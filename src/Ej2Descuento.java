import java.util.Scanner;
//en una tienda se aplica un 20% de descuento a ni�os, 10% para adultos. 
//Tomando en cuenta que se considera ni�o a aquellos que tienen una edad comprendida entre 0 - 17 a�os
public class Ej2Descuento {
	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Introduzca edad: ");
		int edad = entrada.nextInt();
		System.out.print("Precio del producto: ");
		int prec = entrada.nextInt();
		
		
		if(edad > 0 && edad < 18) {
			double desc = prec*0.20;
			double res = prec-desc;
			System.out.print("El Resultado es: "+res);
			
		}else {
		
			double desc = prec*0.10;
			double res = prec-desc;
			System.out.print("El Resultado es: "+res);
		}
	}
}
