
public class EJ6DoWhile {
	public static void main(String[] args) {
		int contador = 0;
		int acum = 0;
		do {
			System.out.println("Contador: "+contador);
			System.out.println("7 en 7: "+acum);
			contador += 1;
			acum += 7;
		}
		while(contador<101);
	}
}
