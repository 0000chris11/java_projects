
public class EJ9Diccionario {
	private int anioPublicacion = 1996;
	/*
	 * Esta funcion retorna la fecha
	 * 
	 * */
	public String retornaFecha() {
		return " 15 de mayo de "+anioPublicacion;
		}
	public String[] [] capitales ={
			{"Chile","Santiago"},
			{"Venenzuela","Caracas"},
			{"Peru","Lima"},
	};
	/*
	 * Esta funcion retorna una ciudad en funcion de un
	 * Pais
	 * */
	public String buscarCapitales(String capital) {
		for(int c=0;c<capitales.length;c++) {
			//System.out.println(capitales.length);
			
			if(capitales[c][0].equals(capital)) {
				return "La ciudad del pais: "+capitales[c][1];
			}
		}
		return "Paiz o ciudad no registrada";
	}
	public String[] [] ciudades ={
			{"Santiago","Santiago es muy linda en privera"},
			{"Caracas","Caracas tiene un clima agradable"},
			{"Lima","Lima es una Ciudad muy calida"},
	};
	/*
	 * Esta Funcion va a retornar la descripcion de una ciudad 
	 * 
	 * */
	public String obtenerDescripcion(String ciudad) {
		for(int c=0;c<ciudades.length;c++) {
			if(ciudades[c][0].equals(ciudad)) {
				return "Descripcion de Ciudad: "+ciudades[c][1];
			}
		}
		return "Ciudad o Descripcion no registrada";
	}
	
	
}
