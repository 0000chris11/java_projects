
public class EJ9Usuario {
	public String nombre;
	//Al crear un objeto igual a la clase
	//*****EJ: Usuario name1 = new Usuario();
	
	public static String apellido;
	//cuando la clase es estatica no es necesario crear un objeto (igual a la clase)
	//*****EJ: Usuario.apellido = "ap";
	
	EJ9Usuario(String nombre,String apellido) {
		this.nombre = nombre;
		this.apellido = apellido;
		
	}
	
}
