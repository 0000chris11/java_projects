import java.util.Scanner;
public class Ej1ParImpar {
	
// pares multiplicar y si son impares dividir
	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		
		System.out.print("Escriba primer numero: ");
		int numero1 = entrada.nextInt();
		
		System.out.print("Escriba segundo numero: ");
		int numero2 = entrada.nextInt();
		
		if(numero1 % 2 == 0 && numero2 % 2 == 0) {
			System.out.print(numero1*numero2);
			}else
				System.out.print(numero1/numero2);
	}
}
