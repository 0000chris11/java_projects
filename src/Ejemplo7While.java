import java.util.Scanner;
public class Ejemplo7While {
	public static void main(String[] args) {
		
		int contador = 0;
		Scanner sc = new Scanner(System.in);
		
		//While(contador<10)
		while(true) {
			contador=contador+1;
			System.out.println("contador: "+contador);
			System.out.println("Escriba 1 para salir: ");
			int opcion = sc.nextInt();
			
			if(opcion==1) {
				break;
			}
		}
	}

}
